import { Component, OnInit, Input } from '@angular/core';
import { DEFAULT_CONFIG, ROLES, LOCAL_STORAGE_KEY } from '../config/Default';
import { AuthService } from '../core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'app';
  //isadmin: boolean;

  constructor(private _authService: AuthService) {
    // if(ROLES.Admin === this._authService.localStorageGetItem(LOCAL_STORAGE_KEY.Role).toLowerCase()){
    //   //this.isadmin = true;
    // }
    // debugger
    // this._authService.getRole(this._authService.localStorageGetItem(LOCAL_STORAGE_KEY.AccessToken)).subscribe(res => {      
    //   if(ROLES.Admin === res["role"].toLowerCase()){
    //     //this.isadmin = true;
    //   }
    // }, error => {      
    // });
    
    this._authService.test().subscribe(res => {
      console.log("success response : " + res);
    }, error => {      
      console.log("error : " + error.message);
    });
  }

  ngOnInit() {
    
  }

  // checkuserrole(value: boolean) {
  //   this.isadmin = value;
  //   console.log("isadmin : " + value);
  // }
}
