import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';



import { DEFAULT_CONFIG, ROLES, LOCAL_STORAGE_KEY } from '../../config/Default';
import { loginJs } from '../../assets/js/main.js';

import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [HttpClient]
})
export class HeaderComponent implements OnInit {
  username: string = "";
  password: string = "";
  errMessage: string = "";

  //@Output() isadmin = new EventEmitter<boolean>();

  constructor(private _http: HttpClient, private _router: Router, private _authService : AuthService) { }

  ngOnInit() {
    loginJs();    
  }

  loginFocus() {
    this.errMessage = "";
  }

  login() {    
    this.errMessage = "checking......";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'username': this.username,
        'password': this.password
      })
    };

    this._http.get(DEFAULT_CONFIG.apiEndPoint + 'auth/token', httpOptions)
      .subscribe(res => { 
        debugger
        console.log(res);
        if (res["access_token"] !== undefined) {
          localStorage.setItem("access_token", res["access_token"]);
          localStorage.setItem("role", res["role"].toLowerCase());
          if (res["role"].toLowerCase() === ROLES.Admin) {
            this._router.navigate(['/admin']);
            //this.isadmin.emit(true);
          }
          loginJs();
          this.errMessage = "";
        }
      }, error => {        
        console.log(error.error);
        this.errMessage = error.error;
      }
      );
  }

}
