import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { ROLES, LOCAL_STORAGE_KEY } from '../../../config/Default';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {  
  //@Output() isadmin = new EventEmitter<boolean>();

  constructor(private _authService:AuthService) {     
    // if(this._authService.localStorageGetItem(LOCAL_STORAGE_KEY.Role).toLocaleLowerCase()===ROLES.Admin){
    //   this.isadmin.emit(true);
    // }
  }

  ngOnInit() {
  }
}
