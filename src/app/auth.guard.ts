import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../core/services/auth.service';
import { LOCAL_STORAGE_KEY, ROLES } from '../config/Default';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private _authService: AuthService,
    private myRoute: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {      
    if (this._authService.isLoggednIn()) {
      console.log("isLoggednIn : "+ this._authService.isLoggednIn());
      if(this._authService.localStorageGetItem(LOCAL_STORAGE_KEY.Role).toLocaleLowerCase()===ROLES.Admin){
        return true;
      }
      else{
        return false;
      }
    } else {
      this.myRoute.navigate([""]);
      return false;
    }
  }

}
