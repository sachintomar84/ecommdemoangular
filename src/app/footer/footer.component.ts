import { Component, OnInit } from '@angular/core';
import {mainJs} from '../../assets/js/main.js';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    mainJs();
  }
}
