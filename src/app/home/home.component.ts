import { Component, OnInit } from '@angular/core';
import slickCustom from '../../assets/js/slick-custom.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {    
    slickCustom();  
  }
}
