import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

import { DEFAULT_CONFIG } from '../../config/Default';
import { RegisterUserModel } from '../models/RegisterUserModel';
import { LoginModel } from '../models/LoginModel';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private myRoute: Router) { }

  register(user: RegisterUserModel): any {
    return this.http.post(DEFAULT_CONFIG.apiEndPoint + 'account/register', user)
      .subscribe(response => console.log(response));
  }

  // getToken(headerOptions: any): any {
  //   return this.http.get(DEFAULT_CONFIG.apiEndPoint + 'auth/token', headerOptions);      
  // }

  localStorageSetItem(key: string, value: string) {
    return localStorage.setItem(key, value);
  }

  localStorageGetItem(key: string) {
    return localStorage.getItem(key);
  }

  localStorageRemoveItem(key: string) {
    return localStorage.removeItem(key);
  }

  isLoggednIn() {
    return this.localStorageGetItem("access_token") !== undefined && this.localStorageGetItem("access_token") !== null;
  }

  logout() {
    this.localStorageRemoveItem("access_token");
    this.myRoute.navigate([""]);
  }

  getRole(token: string): Observable<any> {
    // const httpOptions = { 
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json',
    //   })
    // };
    return this.http.get(DEFAULT_CONFIG.apiEndPoint + 'auth/getrole');
  }

  test(): Observable<any> {    
    return this.http.get(DEFAULT_CONFIG.apiEndPoint + 'admin/test');
  }

}
