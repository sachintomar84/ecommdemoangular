import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DEFAULT_CONFIG, LOCAL_STORAGE_KEY } from '../../config/Default';
import { AuthService } from '../services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private _authService:AuthService){
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let token =this._authService.localStorageGetItem(LOCAL_STORAGE_KEY.AccessToken);
        console.log("token interceptors : " +  token );
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: DEFAULT_CONFIG.token_type + token
                }
            });
        }

        return next.handle(request);
    }
}