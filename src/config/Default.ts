export const DEFAULT_CONFIG = {
    apiEndPoint : 'http://localhost:42868/api/',
    token_type : 'Bearer '
};

export const ROLES = {
    Admin : 'admin',
    User : 'user'
};

export const LOCAL_STORAGE_KEY = {
    AccessToken : 'access_token',
    Role : 'role'        
};